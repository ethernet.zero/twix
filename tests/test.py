#!/usr/bin/env python
# -*- coding: utf-8 -*-

import twix


if __name__ == '__main__':
    import random
    random.seed()

    def test_msgbox_stack(*args, **kwargs):
        style = random.choice([
            'default',
            'error',
            'success',
            'warning',
            'background'
        ])
        app.push_screen(twix.MsgBox(
            'This is a message box with "%s" style. Escape to close.' % style,
            style=style,
            screen_options={
                'align': ('relative', random.randrange(0, 101)),
                'valign': ('relative', random.randrange(0, 101)),
            },
            buttons=[
                ('Open another one!', test_msgbox_stack),
                ('Close', app.pop_screen)
            ]
        ))

    def test_form_builder(*args, **kwargs):
        app.push_screen(
            twix.SimpleFormBuilder({
                'field_one': 'This is a text box.',
                'field_two': 45,
                'field_three': 'This is yet another text box.',
                'checkbox_one': False,
                'checkbox_two': True,
                'checkbox_three': True,
            })
        )

    def test_command_output(*args, **kwargs):
        cob = twix.CommandOutputBox(['ls', '/etc'])
        app.push_screen(cob)
        cob.run()

    app = twix.Main('Twix demo')
    app.set_main_screen(twix.MainScreen(
        twix.MsgBox(
            'This is a standard message box. Press Esc to close.',
            buttons=[
                ('Open another one!', test_msgbox_stack),
                ('Open a simple form', test_form_builder),
                ('Open a command output', test_command_output)
            ]
        ),
    ))
    app.run()
    print app.output
