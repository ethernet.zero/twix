#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import urwid

from twix.dialogs.base import Dialog


class SimpleFileBrowser(Dialog):
    callback = None
    exclusions = []

    def __init__(self, callback=None, *args, **kwargs):
        self.set_callback(callback)
        super(SimpleFileBrowser, self).__init__(*args, **kwargs)

    def refresh_file_list(self):
        self._widgets = []
        while len(self.widget_list) > 0:
            self.widget_list.pop()
        file_list = os.walk(self.props['path'])
        for root, _, files in file_list:
            if 'reverse' in self.props and self.props['reverse'] is True:
                files = list(files)
                files.reverse()
            for file_entry in files:
                if 'exclude' not in self.props \
                   or file_entry not in self.props['exclude']:
                    file_path = os.path.join(root, file_entry)
                    self.menu_entry(
                        file_entry.replace(os.path.sep, '_'),
                        file_path[len(self.props['path']) + 1:],
                        callback=self.callback,
                        params=file_path
                    )

    def build_dialog(self):
        self.screen_options.update({
            'width': ('relative', 20),
            'height': ('relative', 30),
            'min_width': 30,
            'min_height': 20,
        })

        if self.callback is None:
            self.callback = self.select_file

        if self.props['path'].endswith('/'):
            self.props['path'] = self.props['path'][:-1]

        self.refresh_file_list()

        self.dialog_buttons = [
            ('Cancel', self.close_dialog)
        ]

        if 'title' not in self.props:
            self.props['title'] = 'Select file'

    def set_title(self, title):
        self.wrapper.set_title(title)

    def set_callback(self, callback):
        self.callback = callback

    def widget(self):
        return urwid.AttrMap(self.wrapper, 'default')

    def select_file(self, caller, report_file):
        raise NotImplementedError()


class SimpleFormBuilder(Dialog):
    source_data = {}

    def __init__(self, source_data={}, *args, **kwargs):
        self.source_data = source_data
        super(SimpleFormBuilder, self).__init__(*args, **kwargs)

    def build_dialog(self):
        self.screen_options.update({
            'width': ('relative', 30),
            'height': ('relative', 30),
            'min_width': 40,
            'min_height': 20,
        })

        for key, value in self.source_data.iteritems():
            if isinstance(value, (str, unicode)):
                self.edit(key, key, value)
            elif isinstance(value, int):
                self.number_edit(key, key, value)
            elif isinstance(value, bool):
                self.checkbox(key, key, value)

        self.dialog_buttons = [
            ('Save', self.save_form),
            ('Cancel', self.close_dialog)
        ]

    def save_form(self, *args, **kwargs):
        self.source_data.update({
            i['id']: self.get_value(i['id'])
            for i in self._widgets
        })

    def values(self):
        return self.source_data
