#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid
from twix.dialogs.base import Dialog


class MsgBox(Dialog):
    def build(self):
        self.screen_options.update({
            'width': ('relative', 30),
            'height': ('relative', 15),
            'min_width': 15,
            'min_height': 6,
        })
        if 'screen_options' in self.props:
            self.screen_options.update(self.props['screen_options'])
        if 'buttons' in self.props:
            self.dialog_buttons = self.props['buttons']
        else:
            self.dialog_buttons = [
                ('OK', self.close_dialog)
            ]

        self.container = urwid.Filler(urwid.Text(
            self.args[0],
            align=urwid.CENTER
        ))
        dialog_contents = [
            ('weight', 100, self.container),
        ]

        if self.dialog_buttons:
            dialog_contents += [
                ('pack', urwid.Divider(div_char=u"\u2500")),
                ('pack', urwid.Divider()),
                ('pack', urwid.GridFlow(
                    [
                        self.focus_attrmap(urwid.Button(
                            i[0],
                            on_press=i[1],
                            user_data=(i[2] if len(i) > 2 else None)
                        ))
                        for i in self.dialog_buttons
                    ],
                    max([len(i[0]) for i in self.dialog_buttons]) + 4,
                    1,
                    1,
                    urwid.CENTER
                )),
                ('pack', urwid.Divider()),
            ]
        self.wrapper = urwid.LineBox(urwid.Pile(dialog_contents))

        if 'title' in self.props:
            self.wrapper.set_title(self.props['title'])


class ErrorBox(MsgBox):
    def __init__(self, *args, **kwargs):
        super(ErrorBox, self).__init__(style='error', *args, **kwargs)


class SuccessBox(MsgBox):
    def __init__(self, *args, **kwargs):
        super(SuccessBox, self).__init__(style='success', *args, **kwargs)


class WarningBox(MsgBox):
    def __init__(self, *args, **kwargs):
        super(WarningBox, self).__init__(style='warning', *args, **kwargs)


class YesNoBox(MsgBox):
    def __init__(self,
                 callback_yes=None,
                 callback_no=None,
                 params_yes=None,
                 params_no=None,
                 *args, **kwargs):
        buttons = [
            ('Yes', callback_yes, params_yes),
            ('No', callback_no, params_no),
        ]
        super(YesNoBox, self).__init__(buttons=buttons, *args, **kwargs)
