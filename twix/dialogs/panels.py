#!/usr/bin/env python
# -*- coding: utf-8 -*-

from twix.dialogs.msgbox import MsgBox


class MsgPanel(MsgBox):
    def __init__(self, body, *args, **kwargs):
        if 'buttons' in kwargs:
            buttons = kwargs.pop('buttons')
        else:
            buttons = None
        if 'screen_options' in kwargs:
            screen_options = kwargs.pop('screen_options')
        else:
            label_without_spaces = [c for c in body if c == ' ']
            screen_options = {
                'width': ('relative', 10),
                'height': ('relative', 5),
                'min_width': len(body) - len(label_without_spaces) + 6,
                'min_height': 5,
            }
        super(MsgPanel, self).__init__(
            body,
            buttons=buttons,
            screen_options=screen_options,
            *args, **kwargs
        )


class LoadingPanel(MsgPanel):
    def __init__(self, *args, **kwargs):
        super(LoadingPanel, self).__init__('Loading…', *args, **kwargs)
