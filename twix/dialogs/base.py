#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid
import twix


class Dialog(twix.WidgetManager):
    wrapper = None
    container = None
    dialog_buttons = None

    def build(self):
        self.dialog_buttons = []

        self.build_dialog()

        self.container = urwid.ListBox(self.widgets())
        self.enabled_buttons = urwid.GridFlow(
            [
                self.focus_attrmap(urwid.Button(
                    i[0],
                    on_press=i[1],
                    user_data=(i[2] if len(i) > 2 else None)
                ))
                for i in self.dialog_buttons
            ],
            max([len(i[0]) for i in self.dialog_buttons]) + 4,
            1,
            1,
            urwid.CENTER
        )
        self.disabled_buttons = urwid.WidgetDisable(self.enabled_buttons)
        if 'disable_buttons' in self.props and self.props['disable_buttons']:
            self.button_bar = urwid.WidgetPlaceholder(self.disabled_buttons)
        else:
            self.button_bar = urwid.WidgetPlaceholder(self.enabled_buttons)
        self.wrapper = urwid.LineBox(urwid.Pile([
            ('weight', 100, self.container),
            ('pack', urwid.Divider(div_char=u"\u2500")),
            ('pack', urwid.Divider()),
            ('pack', self.button_bar),
            ('pack', urwid.Divider()),
        ]))

        if 'title' in self.props:
            self.wrapper.set_title(self.props['title'])

    def widget(self):
        if 'style' in self.props:
            style = self.props['style']
        else:
            style = 'default'
        return urwid.AttrMap(self.wrapper, style)

    def enable_buttons(self, *args, **kwargs):
        self.button_bar.original_widget = self.enabled_buttons
        self.app.loop.draw_screen()

    def disable_buttons(self, *args, **kwargs):
        self.button_bar.original_widget = self.disabled_buttons
        self.app.loop.draw_screen()

    def toggle_buttons(self, *args, **kwargs):
        self.button_bar.original_widget = \
            self.disabled_buttons \
            if self.button_bar is self.enabled_buttons \
            else self.enabled_buttons
        self.app.loop.draw_screen()

    def close_dialog(self, *args, **kwargs):
        try:
            self.app.pop_screen()
        except BaseException as e:
            self.app.error_msgbox('%s' % e, title='Error')
