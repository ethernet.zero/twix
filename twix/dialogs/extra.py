#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import urwid
import subprocess

from twix.dialogs.base import Dialog


class InputBox(Dialog):
    callback = None
    caption = None
    default_value = None

    def __init__(self,
                 caption,
                 default_value='',
                 callback=None,
                 *args, **kwargs):
        self.caption = caption
        self.default_value = default_value
        self.set_callback(callback)
        super(InputBox, self).__init__(*args, **kwargs)

    def build_dialog(self):
        self.screen_options.update({
            'width': ('relative', 30),
            'height': ('relative', 15),
            'min_width': 15,
            'min_height': 11,
        })

        self.label('caption', self.caption, align=urwid.CENTER)
        self.spacer()
        self.edit(
            'input',
            None,
            value=self.default_value,
            default_value=self.default_value
        )
        self.spacer()

        self.dialog_buttons = [
            ('OK', self.save_form),
            ('Cancel', self.close_dialog)
        ]

    def set_callback(self, callback):
        self.callback = callback

    def save_form(self, *args, **kwargs):
        if self.callback is None:
            self.app.error_msgbox(
                "No callback specified, the value won't be sent anywhere!",
                title='InputBox'
            )
        else:
            self.callback(self.widget(), self.input.value)


class CommandOutputBox(Dialog):
    command = []
    running = False
    return_value = None
    callback = None

    def __init__(self, command, callback=None, *args, **kwargs):
        self.command = command
        self.set_callback(callback)
        if not isinstance(self.command, (list, tuple)):
            raise ValueError('The command must be a list or tuple.')
        super(CommandOutputBox, self).__init__(*args, **kwargs)

    def build_dialog(self):
        self.screen_options.update({
            'width': ('relative', 40),
            'height': ('relative', 40),
            'min_width': 40,
            'min_height': 30,
        })
        if 'screen_options' in self.props:
            self.screen_options.update(self.props['screen_options'])
        if 'buttons' in self.props:
            self.dialog_buttons = self.props['buttons']
        else:
            self.dialog_buttons = [
                ('OK', self.close_dialog)
            ]

        self.props['disable_buttons'] = True

        if 'title' not in self.props:
            self.props['title'] = 'Command output'

    def set_callback(self, callback):
        self.callback = callback

    def run(self, stdin=None):
        try:
            proc = subprocess.Popen(
                self.command,
                stdout=subprocess.PIPE,
                stderr=subprocess.STDOUT,
                stdin=stdin
            )
            self.running = True
            while proc.poll() is None:
                line = proc.stdout.readline()
                self.process_output(line)
            remainder = proc.communicate()[0]
            if remainder:
                self.process_output(remainder, ending=True)
        except:
            err = str(sys.exc_info()[1])
            self.process_output(err)
        finally:
            remainder, stderr = proc.communicate()
            if remainder is None:
                remainder = ''
            if stderr is not None:
                remainder += "\n{}".format(stderr)
            self.return_value = proc.returncode
            self.running = False
            self.process_output(remainder, ending=True)
            self.enable_buttons()
            self.wrapper.original_widget.set_focus(self.button_bar)
            self.app.loop.draw_screen()

    def process_output(self, output, ending=False):
        for line in output.split('\n'):
            self.label('output_%s' % len(self.widget_list), line)
            self.container.set_focus(len(self.widget_list) - 1)
            if len(self.widget_list) > self.wrapper.rows:
                self.widget_list.pop(0)
            self.app.loop.draw_screen()
        if callable(self.callback):
            self.callback(self, output, ending)
