#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid
import math
import time

import twix


class WidgetManager(object):
    args = None
    props = None
    _widgets = None
    widget_list = None
    app = None

    screen_options = None

    def __init__(self, *args, **kwargs):
        self.args = args
        self.props = kwargs
        self._widgets = []
        self.widget_list = urwid.SimpleFocusListWalker(self._widgets)
        self.screen_options = {
            'align': urwid.CENTER,
            'valign': urwid.MIDDLE,
            'width': urwid.RELATIVE_100,
            'height': urwid.RELATIVE_100,
            'min_width': None,
            'min_height': None,
            'left': 0,
            'right': 0,
            'top': 0,
            'bottom': 0,
        }
        self.build()

    def __getitem__(self, key):
        if key not in [i['id'] for i in self._widgets]:
            raise AttributeError("The widget %s doesn't exist." % key)
        widget = [i['widget'] for i in self._widgets if i['id'] == key].pop()
        if isinstance(widget, urwid.AttrMap):
            return widget.original_widget
        else:
            return widget

    __getattr__ = __getitem__

    def add_widget(self,
                   id,
                   widget,
                   value_attr=None,
                   default_value=None,
                   style=None,
                   set_focus_style=False,
                   unfocused_style='unfocused',
                   focused_style='focused'):
        if set_focus_style:
            attr_widget = self.focus_attrmap(
                widget,
                unfocused_style=unfocused_style,
                focused_style=focused_style
            )
        elif style is not None:
            attr_widget = self.style(widget, style)
        else:
            attr_widget = widget
        widget_config = {
            'id': id,
            'widget': attr_widget,
            'value_attr': value_attr,
            'default_value': default_value,
        }
        self._widgets.append(widget_config)
        self.widget_list.append(widget_config['widget'])

    def remove_widget(self, id):
        widgets = [i for i in self._widgets if i['id'] == id]
        if len(widgets) == 1:
            widget_config = widgets.pop()
            self.widget_list.remove(widget_config['widget'])
            self._widgets.remove(widget_config)
        else:
            raise ValueError('You can only remove one widget at a time.')

    def get_value(self, id):
        widget_config = [
            i for i in self._widgets
            if i['id'] == id
        ].pop()
        if 'value_attr' not in widget_config \
           or widget_config['value_attr'] is None:
            return None
        return getattr(self[id], widget_config['value_attr'])

    def set_value(self, id, value):
        widget_config = [
            i for i in self._widgets
            if i['id'] == id
        ].pop()
        if 'value_attr' not in widget_config \
           or widget_config['value_attr'] is None:
            raise AttributeError("This widget doesn't have a value attribute.")
        setattr(self[id], widget_config['value_attr'], value)

    def style(self, wrapped_widget, style='default'):
        return self.focus_attrmap(
            wrapped_widget,
            unfocused_style=style,
            focused_style=None
        )

    def focus_attrmap(self,
                      wrapped_widget,
                      unfocused_style='unfocused',
                      focused_style='focused'):
        return urwid.AttrMap(
            wrapped_widget,
            unfocused_style,
            focus_map=focused_style
        )

    def positioned(self,
                   wrapped_widget,
                   width=urwid.RELATIVE_100,
                   height=urwid.RELATIVE_100,
                   top=0,
                   right=0,
                   bottom=0,
                   left=0,
                   min_width=None,
                   min_height=None,
                   align=urwid.CENTER,
                   valign=urwid.CENTER):
        return urwid.Filler(
            urwid.Padding(
                wrapped_widget,
                width=width,
                left=left,
                right=right,
                min_width=min_width,
                align=align
            ),
            height=height,
            top=top,
            bottom=bottom,
            min_height=min_height,
            valign=valign
        )

    def values(self):
        return {
            i['id']: self.get_value(i['id'])
            for i in self._widgets
        }

    def set_values(self, data):
        for id, value in data.iteritems():
            try:
                self.set_value(id, value)
            except AttributeError:
                pass

    value = property(values, set_values)

    def widgets(self):
        return self.widget_list

    def reset(self):
        for widget in self._widgets:
            if 'value_attr' in widget and widget['value_attr'] is not None:
                self[widget['id']].value = widget['default_value']

    def reset_focus(self):
        widget = self.widget()
        while hasattr(widget, 'original_widget') \
                and not hasattr(widget, 'set_focus'):
            widget = widget.original_widget
        widget.set_focus(0)

    def build(self):
        raise NotImplementedError(
            "Don't instantiate this class directly. " +
            "Use it as a base for your own screens.")

    def widget(self):
        raise NotImplementedError(
            "Don't instantiate this class directly. " +
            "Use it as a base for your own screens.")

    # Widgets

    def label(self,
              id,
              label,
              align=urwid.LEFT,
              label_attribute='default',
              wrap=urwid.SPACE):
        widget = urwid.Text((label_attribute, label), align=align, wrap=wrap)
        self.add_widget(id, widget)
        return widget

    def edit(self,
             id,
             label,
             value='',
             default_value='',
             label_attribute='default'):
        if label is not None and label != '':
            widget = urwid.Edit(
                caption=(label_attribute, '%s: ' % label),
                edit_text=value,
                multiline=True)
        else:
            widget = urwid.Edit(edit_text=value, multiline=True)
        if not hasattr(urwid.Edit, 'value'):
            urwid.Edit.value = property(
                lambda self: self.edit_text,
                lambda self, value: self.set_edit_text(value)
            )
        self.add_widget(
            id,
            widget,
            value_attr='edit_text',
            default_value=default_value,
            set_focus_style=True,
            unfocused_style='edit_unfocused'
        )
        return widget

    def number_edit(self,
                    id,
                    label,
                    value=0,
                    default_value=0,
                    label_attribute='default'):
        if label is not None and label != '':
            widget = urwid.IntEdit(
                caption=(label_attribute, '%s: ' % label),
                default=value
            )
        else:
            widget = urwid.IntEdit(default=value)
        if not hasattr(urwid.IntEdit, 'value'):
            urwid.IntEdit.value = property(
                lambda self: self.value(),
                lambda self, value: self.set_edit_text(value)
            )
        self.add_widget(
            id,
            widget,
            value_attr='edit_text',
            default_value=default_value,
            set_focus_style=True,
            unfocused_style='edit_unfocused'
        )
        return widget

    def button(self, id, label, callback=None, params=None):
        widget = urwid.Button(label, on_press=callback, user_data=params)
        self.add_widget(id, widget, set_focus_style=True)
        return widget

    def menu_entry(self, id, label, callback=None, params=None):
        widget = twix.CustomizableButton(
            label,
            on_press=callback,
            user_data=params,
            left_char='',
            right_char=''
        )
        self.add_widget(id, widget, set_focus_style=True)
        return widget

    def button_bar(self, id, buttons=[]):
        widget = urwid.GridFlow([
            self.focus_attrmap(urwid.Button(
                i[0],
                on_press=i[1],
                user_data=(i[2] if len(i) > 2 else None)))
            for i in buttons
        ], max([len(i[0]) for i in buttons]) + 4, 1, 1, urwid.CENTER)
        self.add_widget(id, widget)
        return widget

    def checkbox(self, id, label, value=False, default_value=False):
        widget = urwid.CheckBox(label, state=value)
        if not hasattr(urwid.CheckBox, 'value'):
            urwid.CheckBox.value = property(
                lambda self: self.state,
                lambda self, value: self.set_state(value)
            )
        self.add_widget(
            id,
            widget,
            value_attr='state',
            default_value=default_value
        )
        return widget

    def radio(self, id, label, group=[], value=False, default_value=False):
        widget = urwid.RadioButton(group, label, state=value)
        if not hasattr(urwid.RadioButton, 'value'):
            urwid.RadioButton.value = property(
                lambda self: self.state,
                lambda self, value: self.set_state(value)
            )
        self.add_widget(
            id,
            widget,
            value_attr='state',
            default_value=default_value
        )
        return widget

    def divider(self):
        widget = urwid.Divider(div_char=u"\u2500")
        existing_dividers = [
            i for i in self._widgets
            if i['id'].startswith('divider_')
        ]
        self.add_widget('divider_%s' % (len(existing_dividers) + 1), widget)
        return widget

    def spacer(self, extra_top=0, extra_bottom=0):
        widget = urwid.Divider(top=extra_top, bottom=extra_bottom)
        existing_spacers = [
            i for i in self._widgets
            if i['id'].startswith('spacer_')
        ]
        self.add_widget('spacer_%s' % (len(existing_spacers) + 1), widget)
        return widget

    def divider_heading(self, title, style=None):
        label = ' %s ' % title
        if style is not None:
            label = (style, label)
        widget = urwid.BoxAdapter(
            urwid.Overlay(
                urwid.Filler(
                    urwid.Text(label)
                ),
                urwid.SolidFill(fill_char=u"\u2500"),
                urwid.CENTER,
                len(title) + 2,
                urwid.MIDDLE,
                1
            ),
            1
        )
        existing_headings = [
            i for i in self._widgets
            if i['id'].startswith('heading_')
        ]
        self.add_widget('heading_%s' % (len(existing_headings) + 1), widget)
        return widget

    def screen(self, id, screen):
        if not isinstance(screen, WidgetManager):
            raise ValueError(
                'The screen should be a subclass of WidgetManager.'
            )
        widget = screen.container
        if not hasattr(widget, 'rows'):
            if isinstance(screen.screen_options['height'], int):
                height = screen.screen_options['height']
            elif isinstance(screen.screen_options['min_height'], int):
                height = screen.screen_options['min_height']
            else:
                height = len(screen._widgets) + 2
            widget = urwid.BoxAdapter(widget, height)
        self.add_widget(id, widget, value_attr='value')


class Main(object):
    __metaclass__ = urwid.MetaSignals

    signals = ['quit', 'keypress']

    palette = [
        ('default', 'default', 'default', '', 'default', 'default'),

        ('background', 'default', 'default', '', 'default', 'default'),
        ('unfocused', 'default', 'default', '', 'default', 'default'),
        (
            'edit_unfocused',
            'default,underline',
            'default',
            '',
            'default,underline',
            'default'
        ),
        (
            'focused',
            'default,standout',
            'default',
            '',
            'default,standout',
            'default'
        ),

        (
            'header',
            'default,standout',
            'default',
            '',
            'default,standout',
            'default'
        ),

        ('error', 'white', 'dark red', '', '#fff', '#800'),
        ('success', 'white', 'dark green', '', '#fff', '#080'),
        ('warning', 'white', 'brown', '', '#fff', '#a80'),
    ]

    output = ''

    main_screen = None

    def __init__(self, name, version=None):
        self.quitting = False
        self.name = name
        self.version = version

        mandatory_styles = (
            'default',
            'background',
            'unfocused',
            'edit_unfocused',
            'focused',
            'header',
            'error',
            'success',
            'warning',
        )

        defined_styles = [i[0] for i in self.palette]
        for style in mandatory_styles:
            if style not in defined_styles:
                raise KeyError(
                    'You must define a palette entry for style "%s".' % style
                )
        WidgetManager.app = self

    def set_main_screen(self, main_screen):
        if not isinstance(main_screen, WidgetManager):
            raise ValueError(
                'The main screen must be a subclass of twix.WidgetManager.'
            )
        self.main_screen = main_screen

    def run(self):
        def handle_input(key):
            if key == 'esc':
                if self.quitting \
                   or not isinstance(
                       self.main_screen.container.body,
                       urwid.Overlay):
                    self.quit()
                else:
                    self.pop_screen()

        if not isinstance(self.main_screen, WidgetManager):
            raise ValueError(
                'The main screen must be a subclass of twix.WidgetManager.'
            )
        self.loop = urwid.MainLoop(
            self.main_screen.widget(),
            self.palette,
            unhandled_input=handle_input
        )
        self.loop.screen.set_terminal_properties(colors=256)
        try:
            self.loop.run()
        except KeyboardInterrupt:
            self.quit()

    # Callbacks/methods

    def quit(self, *args, **kwargs):
        urwid.emit_signal(self, 'quit')
        self.quitting = True
        self.loop.screen.clear()
        raise urwid.ExitMainLoop()

    def switch_screen(self, *args, **kwargs):
        try:
            new_screen = args[1] \
                if isinstance(args[0], urwid.Widget) \
                else args[0]

            if not isinstance(new_screen, WidgetManager):
                raise ValueError(
                    'The screen should be a subclass of WidgetManager.'
                )

            self.main_screen.container.body = new_screen.widget()
        except BaseException as e:
            self.error_msgbox('%s' % e, title='Error')
        self.loop.draw_screen()

    def push_screen(self, *args, **kwargs):
        try:
            new_screen = args[1] \
                if isinstance(args[0], urwid.Widget) \
                else args[0]

            if not isinstance(new_screen, WidgetManager):
                raise ValueError(
                    'The screen should be a subclass of WidgetManager.'
                )

            new_overlay = urwid.Overlay(
                new_screen.widget(),
                self.main_screen.container.body,
                align=new_screen.screen_options['align'],
                width=new_screen.screen_options['width'],
                valign=new_screen.screen_options['valign'],
                height=new_screen.screen_options['height'],
                min_width=new_screen.screen_options['min_width'],
                min_height=new_screen.screen_options['min_height'],
                left=new_screen.screen_options['left'],
                right=new_screen.screen_options['right'],
                top=new_screen.screen_options['top'],
                bottom=new_screen.screen_options['bottom']
            )
            self.main_screen.container.body = new_overlay
        except BaseException as e:
            self.error_msgbox(
                "Couldn't push the screen '%s' onto the stack: %s" % (id, e),
                title='Error pushing screen'
            )
        self.loop.draw_screen()

    def pop_screen(self, *args, **kwargs):
        try:
            if isinstance(self.main_screen.container.body, urwid.Overlay):
                self.main_screen.container.body = \
                    self.main_screen.container.body.contents[0][0]
        except BaseException as e:
            self.error_msgbox(
                "Couldn't pop the topmost screen: %s" % e,
                title='Error popping screen'
            )
        self.loop.draw_screen()

    def pop_all_screens(self):
        while isinstance(self.main_screen.container.body, urwid.Overlay):
            self.pop_screen()

    def flash_message(self, *args, **kwargs):
        try:
            args = list(args)
            if isinstance(args[0], urwid.Widget):
                args.pop(0)
            message = args.pop(0)
            if 'timeout' in kwargs:
                timeout = kwargs.pop('timeout')
            else:
                timeout = 1 + math.floor(len(message) / 20)
            self.push_screen(twix.MsgPanel(message, *args, **kwargs))
            self.loop.draw_screen()
            time.sleep(timeout)
            self.pop_screen()
            if 'callback' in kwargs and callable(kwargs['callback']):
                kwargs['callback'](sender)
        except BaseException as e:
            self.error_msgbox(
                "Couldn't flash message: %s" % e,
                title='Error flashing message'
            )
        self.loop.draw_screen()

    # MsgBox methods

    def msgbox(self, body, *args, **kwargs):
        self.push_screen(twix.MsgBox(body, *args, **kwargs))

    def error_msgbox(self, body, *args, **kwargs):
        self.push_screen(twix.ErrorBox(body, *args, **kwargs))

    def success_msgbox(self, body, *args, **kwargs):
        self.push_screen(twix.SuccessBox(body, *args, **kwargs))

    def warning_msgbox(self, body, *args, **kwargs):
        self.push_screen(twix.WarningBox(body, *args, **kwargs))
