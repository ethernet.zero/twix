#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid
import twix


class MainScreen(twix.WidgetManager):
    container = None
    header = None

    def __init__(self,
                 body,
                 title=None,
                 header_style='header',
                 *args, **kwargs):
        if not isinstance(body, twix.WidgetManager):
            raise ValueError(
                'The body of the main screen must be ' +
                'a subclass of twix.WidgetManager.')
        super(MainScreen, self).__init__(
            body,
            title=title,
            header_style=header_style,
            *args, **kwargs)

    def build(self):
        if self.props['title'] is not None:
            title = self.props['title']
        else:
            title = self.app.name
            if self.app.version is not None:
                title += ' %s' % self.app.version

        self.header = urwid.AttrMap(
            urwid.BoxAdapter(
                urwid.Filler(
                    urwid.Text(
                        title,
                        align=urwid.CENTER
                    )
                ),
                1
            ),
            self.props['header_style']
        )
        self.add_widget('body', self.args[0])

        self.container = urwid.Frame(
            self.body.widget(),
            header=self.header
        )
        self.attrmap_container = urwid.AttrMap(self.container, 'background')

    def widget(self):
        return self.attrmap_container
