#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urwid


class CustomizableButton(urwid.Button):
    def __init__(self,
                 label,
                 on_press=None,
                 user_data=None,
                 left_char='<',
                 right_char='>'):
        self._label = urwid.SelectableIcon("", cursor_position=0)
        cols = urwid.Columns([
            ('fixed', len(left_char), urwid.Text(left_char)),
            self._label,
            ('fixed', len(right_char), urwid.Text(right_char))],
            dividechars=1)
        super(urwid.Button, self).__init__(cols)

        if on_press:
            urwid.connect_signal(self, 'click', on_press, user_data)

        self.set_label(label)
